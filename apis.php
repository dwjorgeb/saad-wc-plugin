<?php

$currency = array(
    'btc' => 'Bitcoin',
    'bch' => 'Bitcoin Cash',
    'ltc' => 'Lite coin',
    'eth' => 'Ethereum',
    'xmr' => 'Monero',
    'iota' => 'IOTA',
);
class apis extends WC_Payment_Gateway
{
    function __construct()
    {
        $this->id = 'cryptApi';

        $this->base_url = 'https://api.cryptapi.io';

        $this->method = 'create';

        $this->invoice = "";

        $this->nonce = "";

        $this->address = "";

        $this->callback = "";

    }
    public function set_invoice($invoice){
        $this->invoice = $invoice;
    }
    public function set_nonce($nonce){
        $this->nonce = $nonce;
    }
    public function set_address($address){
        $this->address = $address;
    }
    public function set_callback($callback){
        $this->callback = $callback;
    }
    public function get_invoice(){
        return $this->invoice;
    }
    public function get_nonce(){
        return $this->invoice;
    }
    public function get_address(){
        return $this->invoice;
    }
    public function get_callback(){
        return $this->callback;
    }

    public function send_request($coin,$method,$address){

        $host = $this->base_url;

        $this->method = $method;

        $this->address = $address;

        $url = $host.'/'.$coin.'/'.$this->method.'/?address='.$this->address.'&callback='.$this->callback;

        $result = file_get_contents($url);

        return json_decode($result);
    }
}