<?php
/*
Plugin Name: CryptAPI Payment Gateway
Description: Cryptocurrency payments on your WooCommerce website
Author: Saad
*/


//check woocomerece plugin is active or not
if ( ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) return;

add_action( 'plugins_loaded', 'crypt_init');

function crypt_init() {

    //make custom class and inherit with parent class
    class WC_CRYPT_SAAD extends WC_Payment_Gateway
    {
        public function __construct()
        {

            $this->id = 'crypt';
            $this->has_fields = false; // if not credit card

            // Custom Payment detail
            $this->method_title = __('Crypt Payment By Saad', 'crypt-saad');
            $this->method_description = __('Crypt Payment Description', 'crypt-saad');

            $this->title = $this->get_option('title');
            $this->description = $this->get_option('description');
            $this->instructions = $this->get_option('instructions', $this->description);

            $this->init_form_fields();
            $this->init_settings();

            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
            add_action('woocommerce_thankyou_' . $this->id, array($this, 'thankyou_page'));
            add_action('woocommerce_api_wc_gateway_' . $this->id, array($this, 'validate_payment'));

            add_action('wp_ajax_nopriv_' . $this->id . '_order_status', array($this, 'order_status'));
            add_action('wp_ajax_' . $this->id . '_order_status', array($this, 'order_status'));

        }

        //create form
        function init_form_fields()
        {
            $this->form_fields = array(

                //checkbox
                'enabled' => array(
                    'title' => __('Enabled', 'crypt-saad'),
                    'type' => 'checkbox',
                    'label' => __('Enable CryptAPI Payments', 'crypt-saad'),
                    'default' => 'yes'
                ),
                //input box
                'title' => array(
                    'title' => __('Title', 'crypt-saad'),
                    'type' => 'text',
                    'description' => __('This controls the title which the user sees during checkout.', 'crypt-saad'),
                    'default' => __('Cryptocurrency', 'crypt-saad'),
                    'desc_tip' => true,
                ),
                //textarea
                'description' 	=> array(
                    'title'       	=> __('Description', 'crypt-saad'),
                    'type'        	=> 'textarea',
                    'default'     	=> __('Pay with cryptocurrency (BTC, BCH, LTC, ETH, XMR and IOTA)', 'crypt-saad'),
                    'description' 	=> __('Payment method description that the customer will see on your checkout', 'crypt-saad' )
                ),

                //multi select dropdown
                'coins' => array(
                    'title' => __('Accepts cryptocurrencies', 'crypt-saad'),
                    'type' => 'multiselect',
                    'default' => '',
                    'css' => 'height: 10rem;',
                    'options' => array(
                        'btc' => 'Bitcoin',
                        'bch' => 'Bitcoin Cash',
                        'ltc' => 'Litecoin',
                        'eth' => 'Ethereum',
                        'xmr' => 'Monero',
                        'iota' => 'IOTA',
                    ),
                    'description' => __("Select which coins do you wish to accept. CTRL + click to select multiple", 'crypt-saad'),
                ),

                //input field
                'bitcoin_address' => array(
                    'title' => __('Bitcoin Address', 'crypt-saad'),
                    'type' => 'text',
                    'description' => __("Insert your Bitcoin address here. Leave blank if you want to skip this cryptocurrency", 'crypt-saad'),
                    'desc_tip' => true,
                ),
                //input field
                'bitcoin_cash_address' => array(
                    'title' => __('Bitcoin Cash Address', 'crypt-saad'),
                    'type' => 'text',
                    'description' => __("Insert your Bitcoin Cash address here. Leave blank if you want to skip this cryptocurrency", 'crypt-saad'),
                    'desc_tip' => true,
                ),
                //input field
                'litecoin_address' => array(
                    'title' => __('Litecoin Address', 'crypt-saad'),
                    'type' => 'text',
                    'description' => __("Insert your Litecoin address here. Leave blank if you want to skip this cryptocurrency", 'crypt-saad'),
                    'desc_tip' => true,
                ),
                //input field
                'ethereum_address' => array(
                    'title' => __('Ethereum Address', 'crypt-saad'),
                    'type' => 'text',
                    'description' => __("Insert your Ethereum address here. Leave blank if you want to skip this cryptocurrency", 'crypt-saad'),
                    'desc_tip' => true,
                ),
                //input field
                'monero_address' => array(
                    'title' => __('Monero Address', 'crypt-saad'),
                    'type' => 'text',
                    'description' => __("Insert your Monero address here. Leave blank if you want to skip this cryptocurrency", 'crypt-saad'),
                    'desc_tip' => true,
                ),
                //input field
                'iota_address' => array(
                    'title' => __('IOTA Address', 'crypt-saad'),
                    'type' => 'text',
                    'description' => __("Insert your IOTA address here. Leave blank if you want to skip this cryptocurrency", 'crypt-saad'),
                    'desc_tip' => true,
                ),
            );
        }



        public function process_payments( $order_id ) {


            $order = wc_get_order( $order_id );

            // Mark as on-hold (we're awaiting the payment)
            $order->update_status( 'on-hold', __( 'Awaiting offline payment', 'wc-gateway-offline' ) );

            // Reduce stock levels
            $order->reduce_order_stock();

            // Remove cart
            WC()->cart->empty_cart();

            return array(
                'result'    => 'success',
                'redirect'  => $this->get_return_url( $order )
            );


        }

        public function thankyou_page() {
            if ( $this->instructions ) {
                echo wpautop( wptexturize( $this->instructions ) );
            }
        }
    }

}

add_filter( 'woocommerce_payment_gateways', 'add_custom_payment_gateway');

function add_custom_payment_gateway( $gateways ) {
    $gateways[] = 'WC_CRYPT_SAAD';
    return $gateways;
}